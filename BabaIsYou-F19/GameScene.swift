//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    let PLAYER_SPEED:CGFloat = 20
    var player = SKSpriteNode()
    
    var wallSmallBlock = SKSpriteNode()
    var iSSmallBlock = SKSpriteNode()
    var stopSmallBlock = SKSpriteNode()
    var flag = SKSpriteNode()
    
    var wall = SKSpriteNode()
    
    var isRulesActive = false;
    
    var wallSize = CGSize(width: 0, height: 0)
    
    var counter = 0
    
    var node1 = SKNode()
    var node2 = SKNode()
    var node3 = SKNode()
    
    var  otherCheck: Bool = false;

    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        
        self.player = self.childNode(withName: "baba") as! SKSpriteNode
        
        self.flag = self.childNode(withName: "flag") as! SKSpriteNode
        
        self.wall = self.childNode(withName: "wall") as! SKSpriteNode
        //self.wall.physicsBody?.collisionBitMask = PhysicalCollisions.NONE
        
        self.wallSmallBlock = self.childNode(withName: "wallblock") as! SKSpriteNode
        self.iSSmallBlock = self.childNode(withName: "isblock") as! SKSpriteNode
        self.stopSmallBlock = self.childNode(withName: "stopblock") as! SKSpriteNode
        
        
    }
   
    func didBegin(_ contact: SKPhysicsContact) {
        print("Something collided!")
        
        
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        
        if (nodeA == nil || nodeB == nil) {
            return
        }
        
        if (nodeA!.name == "wallblock" && nodeB!.name == "isblock") {
            // player die
            print("RESETTING POSITION-AAAA")
            node1 = nodeA!
            node2 = nodeB!
            counter+=1
        }
        if (nodeA!.name == "stopblock" && nodeB!.name == "isblock") {
            print("RESETTING POSITION-BBBBB")
            node3 = nodeA!
            node2 = nodeB!
            counter+=1
        }
        if (nodeA!.name == "wallblock" && nodeB!.name == "isblock1") {
            // player die
            print("RESETTING POSITION-AAAA")
            node1 = nodeA!
            node2 = nodeB!
            counter+=1
        }
        if (nodeA!.name == "stopblock" && nodeB!.name == "isblock1") {
            print("RESETTING POSITION-BBBBB")
            node3 = nodeA!
            node2 = nodeB!
            counter+=1
        }
        if (nodeA!.name == "flagblock" && nodeB!.name == "isblock") {
            // player die
            print("RESETTING POSITION-AAAA")
            node1 = nodeA!
            node2 = nodeB!
            counter+=1
        }
        if (nodeA!.name == "winblock" && nodeB!.name == "isblock") {
            print("RESETTING POSITION-BBBBB")
            node3 = nodeA!
            node2 = nodeB!
            counter+=1
        }
        if (nodeA!.name == "flagblock" && nodeB!.name == "isblock1") {
            // player die
            print("RESETTING POSITION-AAAA")
            node1 = nodeA!
            node2 = nodeB!
            counter+=1
        }
        if (nodeA!.name == "winblock" && nodeB!.name == "isblock1") {
            print("RESETTING POSITION-BBBBB")
            node3 = nodeA!
            node2 = nodeB!
            counter+=1
        }
        if (nodeA!.name == "baba" && nodeB!.name == "flag") {
            print("FLAG")
        }
        if(!otherCheck){
            //otherCheck = true;
            self.player.physicsBody?.collisionBitMask = PhysicalCollisions.BLOCKS
            counter = 0
        }
        
        if counter == 2 {
            print(counter)
            checkTheRule(node1: node1, node2: node2, node3: node3)
        }
        
    
    }
    
    func checkTheRule(node1: SKNode, node2: SKNode, node3: SKNode) {
        counter = 0
        
        checktheIntersectionOfBlocks(node1: node1, node2: node2, node: node3)
        
        
    }
    
    func checktheIntersectionOfBlocks(node1: SKNode,node2: SKNode,node: SKNode){
        
        if (node2.name == "isblock" && node1.name == "wallblock" && node3.name == "stopblock") {
            if (node1.position.x + 70 > node2.position.x && node2.position.x + 70 > node3.position.x) {
                self.player.physicsBody?.collisionBitMask = PhysicalCollisions.BLOCKS | PhysicalCollisions.WALL
            }
            
        }else if(node2.name == "isblock" && node1.name == "flagblock" && node3.name == "winblock") {
        if (node1.position.x + 70 > node2.position.x && node2.position.x + 70 > node3.position.x) {
            self.player.physicsBody?.collisionBitMask = PhysicalCollisions.BLOCKS | PhysicalCollisions.FLAG
            
        }
            
        }
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let mouseTouch = touches.first
        if (mouseTouch == nil) {
            return
        }
        let location = mouseTouch!.location(in: self)

        let nodeTouched = atPoint(location).name

        if (nodeTouched == "upButton") {
            // move up
            self.player.position.y = self.player.position.y + PLAYER_SPEED
        }
        else if (nodeTouched == "downButton") {
            // move down
             self.player.position.y = self.player.position.y - PLAYER_SPEED
        }
        else if (nodeTouched == "leftButton") {
            // move left
             self.player.position.x = self.player.position.x - PLAYER_SPEED
        }
        else if (nodeTouched == "rightButton") {
            // move right
             self.player.position.x = self.player.position.x + PLAYER_SPEED
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    struct PhysicalCollisions {
        static let NONE: UInt32 = 0
        static let BABA: UInt32 = 0b1
        static let BLOCKS: UInt32 = 0b10
        static let WALL: UInt32 = 0b100
        static let FLAG: UInt32 = 0b1000
        
    }
}
